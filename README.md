# fwb

Font Work Bench, `fwb`, is a loosely co-ordinated set of tools to work with fonts.

The _Catalogue_ briefly describes each tool.

The _Snippets_ briefly show ways tools can be configured with options and combined to perform useful tasks.

The _Manual_ thoroughly documents each tool.

The _Matrix_ is a series of papers describing tools, combinations, tasks in a more in-depth and integrated fashion.


## Catalogue

A few of the tools are _external_, in the sense that they are made by someone else.

_ftxinstalledfonts_ is part of the Mac font tools. Runs various
queries (names, unicode characters, languages) on installed
fonts.

_grep_ is part of the original Unix Programming Work Bench which
inspired the name Font Work Bench.
Study and use of `grep` is _essential_.

_hb-view_ is part of harfbuzz and is an essential tool.
It takes a _font file_ and some _text_ and produces an _image_
(usually PNG or SVG).
Related: `pyak-view` which is macOS only and homegrown;
`glox` which shows glyphs (by name) without typesetting them.

_makeotf_ is part of
[AFDKO](https://github.com/adobe-type-tools/afdko);
it is the key tool that makes a binary Open Type Font file from
an existing font file (many formats supported including TTF and UFO)
and feature files.

_otfinfo_ is part of `texlive`.
Useful for inspecting a font file.
Related: `glys` (similar to `otfinfo --glyphs`);
`lifd` (similar to `otfinfo --tables`).

_pyftsubset_ is part of
[fonttools](https://github.com/fonttools/fonttools).
Seems to be primarily written as a subsetter, but
it can also remove hints and possibly other things.

_ttx_ is part of
[fonttools](https://github.com/fonttools/fonttools).
`ttx` converts TrueType and Open Type files to the `.ttx` XML format.
Essential for painless deciphering of binary font files.
It can also convert from XML to binary format, so
it can be useful for making prepared font files.

_tx_ (type exchange) is part of
[AFDKO](https://github.com/adobe-type-tools/afdko);
it is a primitive used by `makeotf`.
It can do various font conversions, and
its PDF mode makes proofs for inspecting glyph by glyph.
A script to compile `tx` directly from the AFDKO sources lurks
in `font8/compiletx`;
this is because AFDKO was taking too long to compile (and required Python).


## Internal tool catalogue

_cajo_, cartesion text sampler for font testing.
Basic and experimental.

_chas_, short for character set. Emits a named character set as a series of (4 or 6 digit) unicode hexademical strings, one per line.

_chum_, show character (Unicode) map; [`chum` source home page](https://git.sr.ht/~drj/chum).
Shows which Unicode codes have an entry in a font file `cmap` table.
Prints one hexadecimal code per line.
(previously known as `ccmap` in `font8`)

_face_, create CSS @font-face fragment for fonts.
[`face` source home page](https://git.sr.ht/~drj/face).
Basic but still useful.

_fext_, short for Feature Extract.
Converts the features in the binary font file to
AFDKO Feature File syntax.
Large and complicated and often produces buggy output, but
still useful for inspecting the rules in a compiled font.

_fwin_, short for Font-Wide Info.
Show header entries and metrics.

_glan_, Glyph Language tool.
Which glyphs for which languages, and which languages use which glyphs?
Very early prototype.

_glox_, short for Glyph Outline Extract.
Convert glyphs in binary font file to SVG.
Has a presentation mode to display off-curve control points.

_glys_, short for Glyph List; [`glys` source home page](https://git.sr.ht/~drj/glys).
List the names of the glyphs in the font-file, one per line,
in the order that the glyphs are stored in the font-file.
(a replacement for `otfinfo --glyphs`)

_lifd_, List Font Table Directory; [`lifd` source home page](https://git.sr.ht/~drj/lifd).
List the tags (and for some tags, versions) of the tables in a
TTF/OTF font-file.
(a replacement for `otfinfo --tables`)

_meti_, Metric Info; [`meti` source home page](https://git.sr.ht/~drj/meti).
List, in CSV format, the glyph metrics from the `hmtx` table
(and in principle the `vmtx` table, but that's not implemented yet).

_namu_, short for Name Unicodes. Searches the `UnicodeData.txt` file for matches, printing each line (or optionally, just the first field, the code) that matches.

_plak_. Prepares a _plaque_ showing a selection of the
characters available in a font.
By default, shows all of the characters in the `cmap` in a random order.

_pyak-view_. Like `hb-view` but uses macOS/AppKit font rendering.

_rettx_. eprocess a TTF file by passing it through ttx:
TTF -ttx-> TTX -ttx-> TTF.
This can be useful for fixing broken TTF files.

_shob_, show bitmap. This shows PNG image files on a terminal
emulator using ANSI colour codes.
Useful for inspecting small images (up to 80 pixels wide)
on the pixel level.

_weft_, join images left-to-right. Simple and not frequently used.
It can be used to join pixel images before converting to vectors
(for example, in Quazatron).
Many tools can do this (tools in PyPNG or netpbm for example),
this one mostly exists so that Font 8 has fewer external
dependencies.


## Snippets

### Show all installed fonts that include Unicode character

    ftxinstalledfonts -f -U2338 | grep YES

From [Michael Boyer on
typedrawers](https://typedrawers.com/discussion/comment/26655/#Comment_26655).

### List all unicode fraction characters

    namu VULGAR

Add `-c` to show codes only.


### Describe character set

Show unicodes and descriptions of all characters
recommend by Hype For Type.

    namu -x "$(chas hft)"


### Describe characters in font

Show unicodes and descriptions of all characters in a font.

    namu -x "$(chum *.otf)"


### Missing characters

Show unicodes and descriptions of
all characters recommended by Monotype that are not in the font-file.

    namu -x "$( chas monotype | grep -v -F -x "$(chum *.otf)")"


### Remove hints from OTF-CFF font

    pyftsubset --no-hinting --glyphs='*' A100LAB20220421-Regular.otf


### PDF proof of all glyphs

320 glyphs per page:

    tx -pdf AirventLAB20220712-Regular.otf > glyphpalette.pdf

As above but followed by large one glyph per page:

    tx -pdf -1 AirventLAB20220712-Regular.otf > glyphpalette.pdf

# END
